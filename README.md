# KalkulatorGUI
This is a project for Java Programming Workshop.

## Project overview
KalkulatorGUI is a calculator that supports basic mathematical operations.   
I wrote it back in early 2017.   
One day I might extend its functions.

## Screenshot
![KalkulatorGUI](screenshot.png)

## How to run it

### Compile
```bash
javac KalkulatorGUI.java
```

### Run
```bash
java KalkulatorGUI
```
or download and run jar file from tags section.