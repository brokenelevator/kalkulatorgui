import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.math.RoundingMode;

class KalkulatorGUI extends JFrame
{
private JPanel buttonPanel = new JPanel();
private JTextField lcd = new JTextField("0");
private JButton przyciski[] = new JButton[16];
private BigDecimal wynik;
Boolean clearLCD = false;
char lastOperator;

KalkulatorGUI()
	{
	super("Kalkulator");
	setSize(250, 320);
	setDefaultCloseOperation(EXIT_ON_CLOSE);
	setLayout(new BorderLayout());
	buttonPanel.setLayout(new GridLayout(4,4));
	add(lcd, BorderLayout.NORTH);
	lcd.setHorizontalAlignment(JTextField.RIGHT);
	lcd.setFont(new Font("Serif", Font.PLAIN, 30));
	lcd.setEditable(false);
	add(buttonPanel, BorderLayout.CENTER);
	String buttonNames[] = {"7", "8", "9", "/", "4", "5", "6", "*", "1", "2", "3", "-", "0", ".", "=", "+"};
	for(int i=0;i<16;i++)
		{
		przyciski[i] = new JButton(buttonNames[i]);
		buttonPanel.add(przyciski[i]);
		}
	for(int i=0;i<13;i++)
		{
		if(i%4 != 3)
			{
			final int CONST_I = i;
			przyciski[i].addActionListener(new ActionListener()
				{
				public void actionPerformed(ActionEvent event)
					{
					if(lcd.getText().equals("0") || clearLCD)
						{
						lcd.setText(przyciski[CONST_I].getLabel());
						clearLCD = false;
						}
					else if(lcd.getText().matches("^-{0,1}[0-9\\.]{1,13}$")) lcd.setText(lcd.getText() + przyciski[CONST_I].getLabel());
					}
				});
			}
		}
	przyciski[3].addActionListener(new OperacjeMatematyczne('\\'));
	przyciski[7].addActionListener(new OperacjeMatematyczne('*'));
	przyciski[11].addActionListener(new OperacjeMatematyczne('-'));
	przyciski[13].addActionListener(new ActionListener()
		{
		public void actionPerformed(ActionEvent event)
			{
			if(lcd.getText().matches("^-{0,1}[0-9]{1,12}$") && !clearLCD) lcd.setText(lcd.getText() + przyciski[13].getLabel());
			else if(clearLCD)
				{
				lcd.setText("0.");
				clearLCD = false;
				}
			}
		});
	przyciski[14].addActionListener(new OperacjeMatematyczne('='));
	przyciski[15].addActionListener(new OperacjeMatematyczne('+'));
	}

	class OperacjeMatematyczne implements ActionListener
	{
	char operator;
	String chainLoadValue = "";
	char chainLoadOperator = ' ';
	
	OperacjeMatematyczne(char newOperator)
		{
		operator = newOperator;
		}
	
	public void actionPerformed(ActionEvent event)
		{
		if(!lcd.getText().matches("^-{0,1}[0-9\\.]{1,}$") || (clearLCD && lastOperator != '=')) return;
		if(lastOperator == '=' && operator == '=')
			{
			wynik = new BigDecimal(lcd.getText());
			lcd.setText(chainLoadValue);
			lastOperator = chainLoadOperator;
			}
		if(wynik == null) wynik = new BigDecimal(lcd.getText());
		else switch(lastOperator)
			{
			case '+':
				wynik = wynik.add(new BigDecimal(lcd.getText()));
				break;
			case '-':
				wynik = wynik.subtract(new BigDecimal(lcd.getText()));
				break;
			case '\\':
				try
					{
					wynik = wynik.divide(new BigDecimal(lcd.getText()), 12, RoundingMode.HALF_EVEN);
					}
				catch(ArithmeticException ae)
					{
					lcd.setText("NaN");
					clearLCD = true;
					wynik = null;
					}
				break;
			case '*':
				wynik = wynik.multiply(new BigDecimal(lcd.getText()));
				break;
			}
		BigDecimal intPart = new BigDecimal(wynik.toBigInteger());
		if(intPart.precision() > 14)
			{
			lcd.setText("ZAKRES");
			clearLCD = true;
			wynik = null;
			return;
			}
		if(intPart.precision() == 14) wynik = wynik.setScale(0, RoundingMode.HALF_EVEN);
		else wynik = wynik.setScale(13 - intPart.precision(), RoundingMode.HALF_EVEN);
		wynik = wynik.stripTrailingZeros();
		if(operator == '=')
			{
			chainLoadValue = lcd.getText();
			chainLoadOperator = lastOperator;
			}
		lcd.setText(wynik.toPlainString());
		if(operator == '=') wynik = null;
		lastOperator = operator;
		clearLCD = true;
		}
	}

public static void main(String[] args)
	{
	KalkulatorGUI okno = new KalkulatorGUI();
	okno.setVisible(true);
	}
}